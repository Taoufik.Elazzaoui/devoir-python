from random import randint

def devin1():
  
    ''' Faire deviner le nombre à l'utilisateur'''

    # Choisir un nombre

    borne_inf, borne_sup = 0,999 # Intervalle
    essai = 1
    print("Je choisis un nombre entre " f"{borne_inf} et {borne_sup}  ") 
    nombre = randint(borne_inf,borne_sup)
    reponse = int(input("Devinez le nombre\nProposition 1 :  "))

    # Traiter les tentatives utilisateur

    while reponse != nombre:
      if reponse<nombre:
        print("C'est plus grand  ")
        essai = essai + 1
      elif reponse>nombre:
        print("C'est plus petit  ")
        essai = essai + 1
      if reponse<borne_inf or reponse>borne_sup:
        print("Un nombre entre " f"{borne_inf} et {borne_sup}  ")
      reponse=int(input(f"Proposition {essai}  "))

    # Afficher le résultat

    if reponse == nombre:
      print(f"Bravo vous avez trouvé en {essai} essais !  ")

def devin2():

    ''' Deviner le nombre de l'utilisateur'''

    # Demander à l'utilisateur de choisir un nombre
	  
    borne_inf, borne_sup = 0,999 
    print("Choisissez un nombre entre " f'{borne_inf} et {borne_sup}')
    nb_essai = 0
	  
    # Rechercher par dichotomie

    reponse = None
    while reponse != 't':   # Chercher tant que la bonne réponse n'est pas trouvée
      essai = (borne_inf + borne_sup)//2
      reponse=input("Est ce qu'il s'agit de " f'{essai} ? trop (g)rand / trop (p)etit / (t)rouvé  ')
      nb_essai = nb_essai + 1

      if reponse == 'p':
        print("Trop petit  ")
        borne_inf = essai
      elif reponse == 'g':
        print("Trop grand  ")
        borne_sup = essai

    # Afficher la bonne réponse

    if reponse == 't':
      print(f"Trouvé en {nb_essai} essais  ")

def devin():

  ''' Donner le choix du jeu à l'utilisateur'''
  
  continuer = 'o'

  # Demander quel choix saisir

  while continuer == 'o':

    tour=int(input(" 1- L'ordianteur choisit un nombre et vous le devinez \n 2- Vous choisissez un nombre et l'ordinateur le devine \n 0- Quitter le programme\n Votre choix :  "))

  # Lancer la partie choisie

    if tour == 1:
      devin1()
    elif tour == 2:
      devin2()
    elif tour == 0:
      continuer = 'n'

  # Continuer ou non de jouer

    if tour != 0:
      continuer =input("Rejouer ?  ")
    elif tour == 0:
      continuer = 'n'
     
  print("Au revoir")


devin()
